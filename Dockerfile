FROM centos:latest
RUN yum install wget -y
ENV JAVA_HOME /usr/lib/jvm/
RUN mkdir -p $JAVA_HOME
WORKDIR $JAVA_HOME
RUN wget --no-cookies --no-check-certificate --header "Cookie: gpw_e24=http%3A%2F%2Fwww.oracle.com%2F; oraclelicense=accept-securebackup-cookie" "http://download.oracle.com/otn-pub/java/jdk/8u181-b13/96a7b8442fe848ef90c96a2fad6ed6d1/jdk-8u181-linux-x64.tar.gz"
RUN ls
RUN tar xzf jdk-8u181-linux-x64.tar.gz
RUN rm jdk-8u181-linux-x64.tar.gz
ENV JAVA_HOME /usr/lib/jvm/jdk1.8.0_181
ENV PATH $PATH:/usr/lib/jvm/jdk1.8.0_181/bin